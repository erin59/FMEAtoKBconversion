# -*- coding: utf-8 -*-
"""
@author: klimashevskaia anastasiia
"""
from openpyxl import Workbook
from openpyxl import load_workbook
import requests
import json
import parser
import mapper
import sys
#####################################
print("Parsing the files...")

if (len(sys.argv) != 2):
    print("You have not entered problem name - running sample example:")
    desc_file = "files/sample.xlsx"
    obs_file = "files/sample.txt"
else:
    desc_file = "files/"+sys.argv[-1]+".xlsx"
    obs_file = "files/"+sys.argv[-1]+".txt"

wb = Workbook()
wb = load_workbook(desc_file)
first_sheet = wb.get_sheet_names()[0]
worksheet = wb.get_sheet_by_name(first_sheet)

comp, modes, props = parser.parse_data(worksheet)
fmea = mapper.build_fmea(comp, modes, props)
hyp = mapper.build_hyp(comp, modes)
mapped_hyp, r_mapped_hyp = mapper.map(hyp)
h_clauses = mapper.build_h_clauses(hyp, props)
causes = mapper.build_causes(h_clauses, props)
m_causes = mapper.map_causes(r_mapped_hyp, causes)

with open(obs_file) as f:
    obs = f.readlines()
obs = [x.rstrip('\n') for x in obs]


#print(str(m_causes), obs, mapped_hyp)
sets = mapper.build_sets(m_causes, obs)

print("Parsed succesfully!\nAssembling computation request...")
request_str = "input={\"sets\": " + str(sets) +"}"
url = "http://pymbd.algorun.org/v1/run"
headers = {'content-type': 'application/x-www-form-urlencoded'}
r = requests.post(url, data = request_str, headers = headers)

if r.status_code == 200:
    print("Computation completed successfully!")
    res = r.text
    json_res = json.loads(res)
    mapped_result = mapper.map_res(json_res["transversals"], mapped_hyp)

    print("Writing the results to the log file...")
    open("files/log.txt", 'w').close()
    with open("log.txt", 'a') as out:
        out.write("Comp set is:\n"+str(comp)+"\n==================\n")
        out.write("Modes set is:\n"+str(modes)+"\n==================\n")
        out.write("Props set is:\n"+str(props)+"\n==================\n")
        out.write("Hyp set is:\n"+str(hyp)+"\n==================\n")
        out.write("Mapped hyp set is:\n"+str(mapped_hyp)+"\n==================\n")
        out.write("Reverse mapped hyp set is:\n"+str(r_mapped_hyp)+"\n==================\n")
        out.write("Clauses set is:\n"+str(h_clauses)+"\n==================\n")
        out.write("Causes set is:\n"+str(causes)+"\n==================\n")
        out.write("Mapped causes set is:\n"+str(m_causes)+"\n******************\n")
        out.write("Observations set is:\n"+str(obs)+"\n==================\n")
        out.write("Sets for HS computation:\n"+str(sets)+"\n==================\n")
        out.write("---SOLUTION---\n")
        out.write("Computation time taken:\n"+str(json_res["timeTaken"]))
        out.write("\nDiagnosis is:\n"+str(mapped_result))
    print("Writing done! Check the log file for your diagnosis.")
else:
    print("ERROR: Request not sent! Please check the request string or connection to the server!")
