# -*- coding: utf-8 -*-
"""
@author: klimashevskaia anastasiia
"""
from openpyxl import Workbook
from openpyxl import load_workbook
import requests
import json

def build_fmea(comp, modes, props):
    fmea = []
    for i, item in enumerate(comp):
        temp = [item, modes[i], props[i]]
        fmea.append(temp)
    return fmea

def build_hyp(comp, modes):
    hyp = []
    for i, item in enumerate(comp):
        temp = [item, modes[i]]
        hyp.append(temp)
    return hyp

def build_h_clauses(hyp, props):
    clauses = []
    for i, item in enumerate(hyp):
        faults = props[i]
        for fault in faults:
            temp = [item, fault]
            clauses.append(temp)
    return clauses

def build_causes(clauses, props):
    causes = dict()
    for clause in clauses:
        if clause[1] not in causes:
            temp = []
            temp.append(clause[0])
            causes[clause[1]]=temp
        else:
            temp = causes[clause[1]]
            temp.append(clause[0])
            causes[clause[1]]=temp
    return causes

def map(hyp):
    dict = {}
    reverse_dict = {}
    count = 1
    for h in hyp:
        dict[count] = h
        reverse_dict[' '.join(h)] = count
        count += 1
    return dict, reverse_dict

def map_causes(dict, causes):
    m_causes = {}
    for key in causes:
        l = []
        for item in causes[key]:
            k = ' '.join(item)
            temp = dict[k]
            l.append(temp)
        m_causes[key] = l
    return m_causes

def build_sets(causes, obs):
    set = []
    for o in obs:
        c = causes[o]
        set.append(list(c))
    return set

def map_res(result, dict):
    res = []
    for s in result:
        temp = []
        for member in s:
            temp.append(dict[member])
        res.append(temp)
    return res

