### Python-based tool for converting a Failure Mode and Effect Analysis problem to Knowledge Base description and further using a hitting set calculation solver for diagnosis.

**Requirements:**
- Python 2.7
- Libraries: openpyxl, requests (can be installed with install.sh)
- Internet connection to perform a request

The tool is using PyMBD Web API, running the library created by Thomas Quaritsch & Ingo Pill.

If you want to run the solver with an example problem:
    
    >>> python main.py
    
To run the solver for custom problem:

    >>> python main.py problem_name
    
**Requirements to the input files:**
- Both files must be named identically!
- Problem files must be stored in "files" folder
- FMEA description is stored in .xlsx file
- Observations are stored in .txt file
- See sample files for organization example
- No whitespaces in effects list after the commas in between!!! 
- Each observation is written on separate line!!!

If the solver performed successfully, all the data would be written in files/log.txt

Project has been written for Modeling Technical Systems course at TU Graz, 2018.
