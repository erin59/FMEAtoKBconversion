# -*- coding: utf-8 -*-
"""
@author: klimashevskaia anastasiia
"""
from openpyxl import Workbook
from openpyxl import load_workbook

def parse_data(worksheet):
    comp = get_comp(worksheet)
    modes = get_modes(worksheet)
    props = get_props(worksheet)
    return comp, modes, props

def get_comp(worksheet):
    comp = []
    for row in worksheet.iter_cols(min_col=1, max_col=1):
        for cell in row:
            comp.append(cell.value)
    return comp

def get_modes(worksheet):
    modes = []
    for row in worksheet.iter_cols(min_col=2, max_col=2):
        for cell in row:
            modes.append(cell.value)
    return modes

def get_props(worksheet):
    props = []
    for row in worksheet.iter_cols(min_col=3, max_col=3):
        for cell in row:
            props.append(cell.value)
    for i, list in enumerate(props):
        props[i] = list.split(",")
    return props
